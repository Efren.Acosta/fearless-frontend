window.addEventListener('DOMContentLoaded', async () => {
    const locationsUrl = 'http://localhost:8000/api/locations/';



    const response = await fetch(locationsUrl);

    if (response.ok) {
        const data = await response.json();
        console.log(data);

        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            //create an 'option' element
            const option = document.createElement('option');

            //set the '.value' property of the option element
            //to the states abbreviation
            option.value = location.id

            //set the '.innerHTML' property of the option ele
            //to the state's name
            option.innerHTML = location.name

            //append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event  => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
