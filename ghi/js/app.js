function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
    <div class="shadow p-3 mb-5 bg-white rounded">
    <div class="pb-auto">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class='card-subtitle mb-2 text-muted'>${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class='card-footer text-muted'>
        ${starts} - ${ends}
        </div>
      </div>
      </div>
      </div>
    `;
}


function createError(){
    return `
    <div class= 'alert alert-danger'>
    Bad Request! </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error('response is invalid.')
        } else {
            const data = await response.json();
            let counter = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const sDate = new Date(details.conference.starts);
                    const starts = sDate.toLocaleDateString();
                    const eDate = new Date(details.conference.ends);
                    const ends = eDate.toLocaleDateString();
                    const location = details.conference.location.name
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelectorAll('.col');
                    column[counter].innerHTML += html;
                    counter += 1;
                    if (counter === column.length) {
                        counter = 0
                    }
                }
            }
        }
    } catch (e) {
        console.error(e)
    }
})
